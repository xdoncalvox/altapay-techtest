terraform {
  backend "s3" {
    bucket         = "bc-techtest-remote"
    key            = "bc-techtest/terraform.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "terraform-obj-lock"
  }
}