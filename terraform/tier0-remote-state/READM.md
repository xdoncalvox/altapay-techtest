This tier creates the base infrastructure required for terraform remote state usage. 


NOTE: The file called backend.tf should be renamed (remove file extention .tf) the first time `terraform init` is executed so in the first run terraform should not use the backend as it will fail due to not existing S3 bucket.