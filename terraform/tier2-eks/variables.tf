variable "region" {
  default     = "eu-west-1"
  description = "AWS region"
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

locals {
  cluster_name = "bc-techtest-${random_string.suffix.result}"
}