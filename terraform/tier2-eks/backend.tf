terraform {
  backend "s3" {
      bucket = "bc-techtest-remote"
      key = "bc-techtest/eks/terraform.tfstate"
      region = "eu-west-1"
      dynamodb_table = "terraform-obj-lock"
  }
}